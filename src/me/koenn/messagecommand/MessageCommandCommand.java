package me.koenn.messagecommand;

import me.koenn.messagecommand.command.CommandLoader;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

/**
 * Copyright (C) Koenn - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Koen Willemse, August 2016
 */
public class MessageCommandCommand implements CommandExecutor {

    private static final String PREFIX = ChatColor.translateAlternateColorCodes('&', "&8&l[&b&lMessageCommand&8&l] ");
    private static final String MESSAGE = PREFIX + "&aRunning MessageCommand version " + MessageCommand.getInstance().getDescription().getVersion();

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        //Check if there are no arguments specified.
        if (args.length < 1) {

            //Send the version message to the CommandSender.
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', MESSAGE));
        } else {

            //Check if the argument is 'reload'.
            if (args[0].equalsIgnoreCase("reload")) {

                //Check if the CommandSender doesn't have the permission.
                if (!sender.hasPermission("messagecommand.reload")) {

                    //Send the no permission message.
                    sender.sendMessage(PREFIX + ChatColor.RED + "You don't have permission to use this command!");
                    return true;
                }

                //Reload the config and the CustomCommands.
                MessageCommand.getInstance().reloadConfig();
                CommandLoader.loadCommands(MessageCommand.getInstance().getConfig());

                //Send the succeed message.
                sender.sendMessage(PREFIX + ChatColor.GREEN + "Successfully reloaded all commands!");
            } else {

                //Send the unknown command message.
                sender.sendMessage(PREFIX + ChatColor.RED + "Unknown command \'" + args[0] + "\'");
            }
        }
        return true;
    }
}
