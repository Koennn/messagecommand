package me.koenn.messagecommand.command;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) Koenn - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Koen Willemse, August 2016
 */
public class CustomCommand implements Listener {

    /**
     * List of all registered CustomCommands.
     */
    public static final List<CustomCommand> customCommands = new ArrayList<>();

    /**
     * Command which triggers the listener.
     */
    private final String command;

    /**
     * Message to send when the Command is executed.
     */
    private final String message;

    /**
     * Constructor for a CustomCommand using the command and message.
     *
     * @param command Command which triggers the listener
     * @param message Message to send when the Command is executed
     */
    public CustomCommand(String command, String message) {
        this.command = command;
        this.message = ChatColor.translateAlternateColorCodes('&', message);
        customCommands.add(this);
    }

    @EventHandler
    public void onPlayerCommandPreprocess(PlayerCommandPreprocessEvent event) {
        //Check if the CustomCommand isn't registered.
        if (!customCommands.contains(this)) {

            //Don't handle the command.
            return;
        }

        //Make local variables.
        String command = event.getMessage().split(" ")[0].replace("/", "");
        Player player = event.getPlayer();

        //Check if the executed command matches the one in the command field.
        if (command.equalsIgnoreCase(this.command)) {

            //Send the message to the Player.
            player.sendMessage(this.message);

            //Cancel the event.
            event.setCancelled(true);
        }
    }
}
