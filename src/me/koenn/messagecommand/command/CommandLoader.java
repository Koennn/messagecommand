package me.koenn.messagecommand.command;

import me.koenn.messagecommand.MessageCommand;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;

/**
 * Copyright (C) Koenn - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Koen Willemse, August 2016
 */
public final class CommandLoader {

    public static void loadCommands(FileConfiguration config) {
        //Make local variables.
        ConfigurationSection commands = config.getConfigurationSection("commands");
        int commandAmount = 0;

        //Clear all existing CustomCommands.
        CustomCommand.customCommands.clear();

        //Loop over all entries in the 'commands' section.
        for (String command : commands.getKeys(false)) {

            //Get the message for the command.
            String message = commands.getString(command);

            //Create and register the CustomCommand object.
            Bukkit.getPluginManager().registerEvents(new CustomCommand(command, message), MessageCommand.getInstance());

            //Increase the commandAmount.
            commandAmount++;
        }

        //Log the succeed message to the console.
        MessageCommand.log("Successfully loaded " + commandAmount + " commands!");
    }
}
