package me.koenn.messagecommand;

import me.koenn.messagecommand.command.CommandLoader;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

/**
 * Copyright (C) Koenn - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Koen Willemse, August 2016
 */
public final class MessageCommand extends JavaPlugin {

    /**
     * Static instance of this plugin class.
     */
    private static Plugin instance;

    /**
     * Get the instance of this class.
     *
     * @return Plugin instance
     */
    public static Plugin getInstance() {
        return instance;
    }

    /**
     * Simple logger method to log a message to the console.
     *
     * @param msg Message to log
     */
    public static void log(String msg) {
        instance.getLogger().info(msg);
    }

    /**
     * Plugin enable method.
     * Gets called when the plugin enables
     */
    @Override
    public void onEnable() {
        //Set static variables.
        instance = this;

        log("All credits for this plugin go to Koenn");

        //Get the current Bukkit version.
        String bukkitVersion = Bukkit.getServer().getBukkitVersion();

        //Check if the bukkitVersion is not 1.8, 1.9 or 1.10.
        if (!bukkitVersion.contains("1.8") && !bukkitVersion.contains("1.9") && !bukkitVersion.contains("1.10")) {

            //Log the error message.
            this.getLogger().severe("#####################################################");
            this.getLogger().severe("This plugin is only compatible with 1.8, 1.9 and 1.10");
            this.getLogger().severe("#####################################################");

            //Disable the plugin.
            Bukkit.getPluginManager().disablePlugin(this);

            //Stop the onEnable method.
            return;
        }

        //Log the bukkitVersion.
        log("Loading plugin for minecraft version " + bukkitVersion + "!");

        //Get the /messagecommand Command and set the CommandExecutor.
        this.getCommand("messagecommand").setExecutor(new MessageCommandCommand());

        //Create the plugin folder if it doesn't exist.
        if (!this.getDataFolder().exists()) {
            this.getDataFolder().mkdir();
        }

        //Create the config file if it doesn't exist.
        if (!new File(this.getDataFolder(), "config.yml").exists()) {
            this.saveDefaultConfig();
        }

        //Load the commands from the config file.
        CommandLoader.loadCommands(this.getConfig());
    }
}
